from setuptools import setup

setup(
    version='1.5',
    name='qweborf',
    author="Salvo 'LtWorf' Tomaselli",
    author_email='tiposchi@tiscali.it',
    maintainer="Salvo 'LtWorf' Tomaselli",
    maintainer_email='tiposchi@tiscali.it',
    url='https://ltworf.codeberg.page/weborf/',
    license='GPL3',
    packages=('qweborf',),
    )

